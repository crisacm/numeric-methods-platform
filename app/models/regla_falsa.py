from prettytable import PrettyTable
from math import *


class ReglaFalsa:
    function = ''
    value_a = 0
    value_b = 0
    iterations = 5
    decimals = 2

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    def evaluate_xi(self, fa, fb):
        try:
            a = self.value_a
            b = self.value_b
            function = '(a * fb - b * fa) / (fb - fa)'
            return eval(function)
        except (ValueError, NameError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate_xi()]:exception: ', ex)
            return False

    def method(self):
        data = []
        for it in range(self.iterations):
            fa_tmp = self.evaluate(self.function, self.value_a)
            if not fa_tmp:
                fa = 0
            else:
                fa = round(fa_tmp, self.decimals)
            fb_tmp = self.evaluate(self.function, self.value_b)
            if not fb_tmp:
                fb = 0
            else:
                fb = round(fb_tmp, self.decimals)
            xi_tmp = self.evaluate_xi(fa, fb)
            if not xi_tmp:
                xi = 0
            else:
                xi = round(xi_tmp, self.decimals)
            fxi_tmp = self.evaluate(self.function, xi)
            if not fxi_tmp:
                fxi = 0
            else:
                fxi = round(fxi_tmp, self.decimals)
            data.append([it, self.value_a, self.value_b, fa, fb, xi, fxi])
            if fa > 0 and fb > 0:
                break_state = 1
                break
            elif fa < 0 and fb < 0:
                break_state = 1
                break
            else:
                if fxi >= 0 and fa >= 0:
                    self.value_a = round(xi, self.decimals)
                    self.value_b = round(self.value_b, self.decimals)
                elif fxi >= 0 and fb >= 0:
                    self.value_a = round(self.value_a, self.decimals)
                    self.value_b = round(xi, self.decimals)
                elif fxi < 0 and fa < 0:
                    self.value_a = round(xi, self.decimals)
                    self.value_b = round(self.value_b, self.decimals)
                elif fxi < 0 and fb < 0:
                    self.value_a = round(self.value_a, self.decimals)
                    self.value_b = round(xi, self.decimals)
        return data

    def show(self):
        self.function = '2 * sin(x) - pow(x, -3)'
        self.value_a = 1
        self.value_b = -2
        data = self.method()
        table = PrettyTable(['it', 'A', 'B', 'Fa', 'Fb', 'xi', 'Xi'])
        for i in range(len(data)):
            table.add_row([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6]])
        print(table)


if __name__ == "__main__":
    regla_falsa = ReglaFalsa()
    regla_falsa.show()
