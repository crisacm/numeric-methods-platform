import json
from math import *


class Trapecio:
    data = []
    json = ''

    def __init__(self, a, b, f, n, decimals):
        self.a = a
        self.b = b
        self.f = f
        self.n = n
        self.decimals = decimals

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    def solve_method(self):
        h = round((self.b - self.a) / self.n, self.decimals)
        data = []
        final_str = ''
        final_str_solve = ''
        solve = 0

        first_it = round(self.evaluate(self.f, self.a), self.decimals)
        final_str += '(h/2) + f(a)'
        final_str_solve += '({0}/2) + f({1})'.format(h, self.a)
        solve = solve + first_it

        for i in range(int(self.n + 1)):
            if 0 < i < 6:
                op_h = '{0} * {1}'.format(i, h)
                solve_op_h = eval(op_h)
                op_a = '{0} + {1}'.format(self.a, solve_op_h)
                solve_op_a = eval(op_a)
                f_solved = self.evaluate(self.f, solve_op_a)
                it = '{0} * {1}'.format(2, f_solved)
                it_solved = self.evaluate(it, 0)

                final_str += (' [{0}f(a + {1}h)]'.format(2, i))
                final_str_solve += ' + [{0}f({1} + {2} * {3})]'.format(2, self.a, i, h)
                solve = solve + it_solved

        last_it = round(self.evaluate(self.f, self.b), self.decimals)

        final_str += ' f(b)'
        final_str_solve += ' + f({0})'.format(self.b)

        solve = solve + last_it
        final_solve = round((h / 2) * solve, self.decimals)

        data.append(final_str)
        data.append(final_str_solve)
        data.append(final_solve)

        self.data = data
        self.json = json.dumps({
            'functions': final_str,
            'functions_solve': final_str_solve,
            'final_solve': final_solve
        }, indent=4, sort_keys=True)

    def show(self):
        print(self.data)
        print(self.json)
