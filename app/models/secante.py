from prettytable import PrettyTable
from math import *
import json


class Secante:
    data = []
    json = ''

    def __init__(self, x0, x1, f, n, decimals):
        self.x0 = x0
        self.x1 = x1
        self.f = f
        self.n = n
        self.decimals = decimals

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    @staticmethod
    def evaluate_n(x0, x1, fx0, fx1):
        try:
            return eval('x1 - (x1-x0)*fx1/(fx1-fx0) ')
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate_xi()]:exception: ', ex)
            return False

    def solve_method(self):
        data = []
        dic = dict()

        for it in range(self.n):
            fx0 = round(self.evaluate(self.f, self.x0), self.decimals)
            fx1 = round(self.evaluate(self.f, self.x1), self.decimals)
            x = round(self.evaluate_n(self.x0, self.x1, fx0, fx1), self.decimals)

            data.append([it, x, abs(x - self.x1)])
            dic[it] = [it, x, abs(x - self.x1)]

            self.x0 = self.x1
            self.x1 = x

        self.data = data
        self.json = json.dumps({
            'data': json.dumps(self.data)
        }, indent=4, sort_keys=True)

    def example(self):
        self.f = 'pow(x, 3) + 4 * pow(x, 2) - 10'
        self.x0 = 1
        self.x1 = 2
        self.n = 5
        self.decimals = 2
