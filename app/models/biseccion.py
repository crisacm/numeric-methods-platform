from prettytable import PrettyTable
from math import *


class Biseccion:
    function = ''
    value_a = 0
    value_b = 0
    iterations = 5
    decimals = 2

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    def method(self):
        data = []
        for it in range(int(self.iterations)):
            fa_tmp = self.evaluate(self.function, self.value_a)
            if not fa_tmp:
                fa = 0
            else:
                fa = round(fa_tmp, self.decimals)
            fb_tmp = self.evaluate(self.function, self.value_b)
            if not fb_tmp:
                fb = 0
            else:
                fb = round(fb_tmp, self.decimals)
            xi_tmp = (self.value_a + self.value_b) / 2
            if not xi_tmp:
                xi = 0
            else:
                xi = round(xi_tmp, self.decimals)
            fxi_temp = self.evaluate(self.function, xi)
            if not fxi_temp:
                fxi = 0
            else:
                fxi = round(fxi_temp, self.decimals)
            data.append([it, self.value_a, self.value_b, fa, fb, xi, fxi])
            if fa > 0 and fb > 0:
                break
            elif fa < 0 and fb < 0:
                break
            else:
                if fxi >= 0 and fa >= 0:
                    self.value_a = round(xi, self.decimals)
                    self.value_b = round(self.value_b, self.decimals)
                elif fxi >= 0 and fb >= 0:
                    self.value_a = round(self.value_a, self.decimals)
                    self.value_b = round(xi, self.decimals)
                elif fxi < 0 and fa < 0:
                    self.value_a = round(xi, self.decimals)
                    self.value_b = round(self.value_b, self.decimals)
                elif fxi < 0 and fb < 0:
                    self.value_a = round(self.value_a, self.decimals)
                    self.value_b = round(xi, self.decimals)
        return data

    def show(self):
        self.function = '2 * x + sin(x) - 3 * pow(x, -2)'
        self.value_a = 1
        self.value_b = 2
        data = self.method()
        table = PrettyTable(['it', 'A', 'B', 'Fa', 'Fb', 'xi', 'Xi'])
        for i in range(len(data)):
            table.add_row([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6]])
        print(table)
