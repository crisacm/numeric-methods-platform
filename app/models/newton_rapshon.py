from prettytable import PrettyTable
from math import *


class NewtonRapshon:
    function = ''
    derivated_function = ''
    value_xi = 0
    value_fxi = 0
    value_fdxi = 0
    iterations = 5
    decimals = 2

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    @staticmethod
    def evaluate_xi(xi, fxi, fdxi):
        try:
            function = '(xi) - (fxi / fdxi)'
            return eval(function)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate_xi()]:exception: ', ex)
            return False

    def method(self):
        data = []
        for it in range(self.iterations):
            if it == 0:
                fxi_temp = self.evaluate(self.function, round(self.value_xi, self.decimals))
                self.value_fxi = round(fxi_temp, self.decimals)
                fdxi_temp = self.evaluate(self.derivated_function, round(self.value_xi, self.decimals))
                self.value_fdxi = round(fdxi_temp, self.decimals)
                xi_less_xi = 0
                data.append([it, round(self.value_xi, self.decimals), self.value_fxi, self.value_fdxi, xi_less_xi])
            elif it > 0:
                xi_temp = self.evaluate_xi(round(self.value_xi, self.decimals), self.value_fxi, self.value_fdxi)
                if not xi_temp:
                    xi_temp = 0
                xi_less_xi = self.value_xi - round(xi_temp, self.decimals)
                if xi_temp:
                    self.value_xi = round(xi_temp, self.decimals)
                fxi_temp = self.evaluate(self.function, self.value_xi)
                if not fxi_temp:
                    fxi_temp = 0
                self.value_fxi = round(fxi_temp, self.decimals)
                fdxi_temp = self.evaluate(self.derivated_function, self.value_xi)
                if not fdxi_temp:
                    fdxi_temp = 0
                self.value_fdxi = round(fdxi_temp, self.decimals)
                data.append([it, self.value_xi, self.value_fxi, self.value_fdxi, abs(xi_less_xi)])
        return data

    def show(self):
        self.function = 'pow(x, 3) + 4 * pow(x, 2) - 10'
        self.derivated_function = '3 * pow(x, 2) + 8 * x'
        self.value_xi = 0.75
        data = self.method()
        table = PrettyTable(['it', 'xi', 'Fxi', "F'Xi", '|Xi - X(i-1)|'])
        for i in range(len(data)):
            table.add_row([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4]])
        print(table)


if __name__ == "__main__":
    newton_rapshon = NewtonRapshon()
    newton_rapshon.show()
