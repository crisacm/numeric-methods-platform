from prettytable import PrettyTable
import json
from math import *


class Jacobi:
    func_x = ''
    func_y = ''
    func_z = ''
    x = 0
    y = 0
    z = 0
    error = 0.0001
    decimals = 2

    data = []
    json = ''

    @staticmethod
    def evaluate(fun, x, y, z):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    @staticmethod
    def evaluate_error(x0, y0, z0, x1, y1, z1):
        try:
            func = 'sqrt(pow((x1 - x0), 2) + pow((y1 - y0), 2) + pow((z1 - z0), 2))'
            return eval(func)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate_error()]:exception: ', ex)
            return False

    def method(self):
        data = []
        error = 1
        it = 0
        while error > self.error:
            x0 = self.x
            y0 = self.y
            z0 = self.z
            fx = round(self.evaluate(self.func_x, x0, y0, z0), self.decimals)
            fy = round(self.evaluate(self.func_y, x0, y0, z0), self.decimals)
            fz = round(self.evaluate(self.func_z, x0, y0, z0), self.decimals)
            x1 = fx
            y1 = fy
            z1 = fz
            error = self.evaluate_error(x0, y0, z0, x1, y1, z1)
            data.append([it, fx, fy, fz, error])
            self.x = x1
            self.y = y1
            self.z = z1
            it += 1

        self.data = data
        self.json = json.dumps({
            'final_solved': json.dumps(data, indent=4, sort_keys=True)
        }, indent=4, sort_keys=True)

    def example(self):
        self.func_x = '(3 + 2 * y - z) / 5'
        self.func_y = '(x - 3 * z - 2) / -7'
        self.func_z = '(1 - 2 * x + y) / 8'
        self.x = 0
        self.y = 0
        self.z = 0
        self.error = 0
        self.decimals = 5

    def show(self):
        self.func_x = '(3 + 2 * y - z) / 5'
        self.func_y = '(x - 3 * z - 2) / -7'
        self.func_z = '(1 - 2 * x + y) / 8'
        self.x = 0
        self.y = 0
        self.z = 0
        self.error = 0
        self.decimals = 5
        data = self.method()
        table = PrettyTable(['n', 'Xn', 'Yn', 'Zn', 'Error'])
        for i in range(len(data)):
            table.add_row([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4]])
        print(table)


if __name__ == "__main__":
    jacobi = Jacobi()
    jacobi.example()
    jacobi.show()
