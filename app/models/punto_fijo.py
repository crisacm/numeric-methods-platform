from math import *
import json


class PuntoFijo:
    data = []
    json = ''

    def __init__(self, a, f, g, e, decimals):
        self.a = a
        self.f = f
        self.g = g
        self.e = e
        self.decimals = decimals

    @staticmethod
    def evaluate(fun, x):
        try:
            return eval(fun)
        except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
            print('[ERROR][evaluate()]:exception: ', ex)
            return False

    def solve_method(self):
        data = []
        rng = 1000

        for i in range(rng):
            x_obt = round(self.evaluate(self.g, self.a), self.decimals)
            error = round(abs(x_obt - self.a), self.decimals)
            data.append([self.a, x_obt, error])
            if x_obt == 0:
                break
            elif error <= self.e:
                break
            else:
                self.a = x_obt

        self.data = data
        self.json = json.dumps({
            'final_solved': json.dumps(data)
        }, indent=4, sort_keys=True)

    def example(self):
        self.a = 1.85
        self.f = 'cos(pow(x, 2))'
        self.g = '-2 * x * sin(pow(x, 2))'
        self.e = 0.4
        self.decimals = 4
