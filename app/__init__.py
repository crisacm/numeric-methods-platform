from flask import Flask, render_template, request, flash
from app.models.biseccion import Biseccion
from app.models.regla_falsa import ReglaFalsa
from app.models.newton_rapshon import NewtonRapshon
from app.models.secante import Secante
from app.models.punto_fijo import PuntoFijo
from app.models.jacobi import Jacobi
from app.models.gauss_seidel import GaussSeidel
from app.models.trapecio import Trapecio
import json
from math import *

application = Flask(__name__)


def validate(fun, x):
    try:
        eval(fun)
        return True
    except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
        print('[ERROR][validate()]:exception: ', ex)
        return False


@application.errorhandler(404)
def not_found(error):
    return render_template('_404.html', error=error), 404


@application.route('/')
def main():
    return render_template('index.html')


@application.route('/biseccion', methods=['GET', 'POST'])
def biseccion():
    if request.method == 'POST':
        var_biseccion = Biseccion()
        var_biseccion.function = request.form.get('function')
        var_biseccion.value_a = float(request.form.get('value-a'))
        var_biseccion.value_b = float(request.form.get('value-b'))
        var_biseccion.iterations = int(request.form.get('iterations'))
        var_biseccion.decimals = int(request.form.get('decimals'))
        solve_info = [var_biseccion.function, var_biseccion.value_a, var_biseccion.value_b,
                      var_biseccion.iterations, var_biseccion.decimals]

        if validate(var_biseccion.function, 1):
            table = var_biseccion.method()
            return render_template('biseccion.html', solve=True, table=table, table_len=len(table),
                                   solve_info=solve_info)
        else:
            msg = 'La función ', var_biseccion.function, ' no es valida'
            flash(msg, 'warning')
            return render_template('biseccion.html', solve=False, solfe_info=solve_info)
    else:
        return render_template('biseccion.html', solve=False)


@application.route('/regla-falsa', methods=['GET', 'POST'])
def regla_falsa():
    if request.method == 'POST':
        var_regla_falsa = ReglaFalsa()
        var_regla_falsa.function = request.form.get('function')
        var_regla_falsa.value_a = float(request.form.get('value-a'))
        var_regla_falsa.value_b = float(request.form.get('value-b'))
        var_regla_falsa.iterations = int(request.form.get('iterations'))
        var_regla_falsa.decimals = int(request.form.get('decimals'))
        solve_info = [var_regla_falsa.function, var_regla_falsa.value_a, var_regla_falsa.value_b,
                      var_regla_falsa.iterations, var_regla_falsa.decimals]

        if validate(var_regla_falsa.function, 1):
            table = var_regla_falsa.method()
            return render_template('regla-falsa.html', solve=True, table=table, table_len=len(table),
                                   solve_info=solve_info)
        else:
            msg = 'La función ', var_regla_falsa.function, ' no es valida'
            flash(msg, 'warning')
            return render_template('regla-falsa.html', solve=False, solfe_info=solve_info)
    else:
        return render_template('regla-falsa.html', solve=False)


@application.route('/newton-rapshon', methods=['GET', 'POST'])
def newton_rapshon():
    if request.method == 'POST':
        var_newton_rapshon = NewtonRapshon()
        var_newton_rapshon.function = request.form.get('function')
        var_newton_rapshon.derivated_function = request.form.get('function-derivate')
        var_newton_rapshon.value_xi = float(request.form.get('value-xi'))
        var_newton_rapshon.iterations = int(request.form.get('iterations'))
        var_newton_rapshon.decimals = int(request.form.get('decimals'))
        solve_info = [var_newton_rapshon.function, var_newton_rapshon.derivated_function, var_newton_rapshon.value_xi,
                      var_newton_rapshon.iterations, var_newton_rapshon.decimals]

        if validate(var_newton_rapshon.function, 1):
            table = var_newton_rapshon.method()
            return render_template('newton-rapshon.html', solve=True, table=table, table_len=len(table),
                                   solve_info=solve_info)
        else:
            msg = 'La función ', var_newton_rapshon.function, ' no es valida'
            flash(msg, 'warning')
            return render_template('newton-rapshon.html', solve=False, solfe_info=solve_info)
    else:
        return render_template('newton-rapshon.html', solve=False)


@application.route('/secante', methods=['GET', 'POST'])
def secante():
    if request.method == 'POST':
        values = [
            request.form.get('value-x0'),
            request.form.get('value-x1'),
            request.form.get('function'),
            request.form.get('iterations'),
            request.form.get('decimals')
        ]

        s = Secante(
            int(values[0]),
            int(values[1]),
            str(values[2]),
            int(values[3]),
            int(values[4])
        )
        s.solve_method()

        solve_info = []
        table = s.data
        table_len = len(table)
        return render_template('secante.html', solve=True, table=table, table_len=table_len, solve_info=solve_info)
    else:
        return render_template('secante.html', solve=False)


@application.route('/punto-fijo', methods=['GET', 'POST'])
def punto_fijo():
    if request.method == 'POST':
        values = [
            request.form.get('value-init'),
            request.form.get('function'),
            request.form.get('function-g'),
            request.form.get('value-error'),
            request.form.get('decimals')
        ]

        p = PuntoFijo(
            float(values[0]),
            str(values[1]),
            str(values[2]),
            float(values[3]),
            int(values[4])
        )
        p.solve_method()

        solve_info = [p.f, p.g, p.a, p.decimals, p.e]
        error = p.evaluate(p.f, 0)
        table = p.data
        table_len = len(table)
        return render_template('punto-fijo.html', solve=True, table=table, solve_info=solve_info, error=error, table_len=table_len)
    else:
        return render_template('punto-fijo.html', solve=False)


def validate_jacobi(fun, x, y, z):
    try:
        eval(fun)
        return True
    except (ValueError, NameError, SyntaxError, ZeroDivisionError) as ex:
        print('[ERROR][validate()]:exception: ', ex)
        return False


@application.route('/jacobi', methods=['GET', 'POST'])
def jacobi():
    if request.method == 'POST':
        j = Jacobi()
        j.func_x = request.form.get('function-x')
        j.func_y = request.form.get('function-y')
        j.func_z = request.form.get('function-z')
        j.x = float(request.form.get('value-x'))
        j.y = float(request.form.get('value-y'))
        j.z = float(request.form.get('value-z'))
        j.error = float(request.form.get('value-error'))
        j.decimals = int(request.form.get('decimals'))
        solve_info = [j.func_x, j.func_y, j.func_z, j.x, j.y, j.z, j.error, j.decimals]
        print('LOG: Llego hast aaquí')
        if validate_jacobi(j.func_x, j.x, j.y, j.z):
            valid = True
        elif validate_jacobi(j.func_y, j.x, j.y, j.z):
            valid = True
        elif validate_jacobi(j.func_z, j.x, j.y, j.z):
            valid = True
        else:
            valid = False
        if valid:
            table = j.method()
            table_len = len(table)
            return render_template('jacobi.html', solve=True, table=table, solve_info=solve_info,
                                   table_len=table_len)
        else:
            msg = 'Una de las funciónes no es valida'
            flash(msg, 'warning')
            return render_template('jacobi.html', solve=False, solfe_info=solve_info)
    else:
        return render_template('jacobi.html', solve=False)


@application.route('/gauss-seidel', methods=['GET', 'POST'])
def gauss_seidel():
    if request.method == 'POST':
        g = GaussSeidel()
        g.func_x = request.form.get('function-x')
        g.func_y = request.form.get('function-y')
        g.func_z = request.form.get('function-z')
        g.x = float(request.form.get('value-x'))
        g.y = float(request.form.get('value-y'))
        g.z = float(request.form.get('value-z'))
        g.error = float(request.form.get('value-error'))
        g.decimals = int(request.form.get('decimals'))
        solve_info = [g.func_x, g.func_y, g.func_z, g.x, g.y, g.z, g.error, g.decimals]
        if validate_jacobi(g.func_x, g.x, g.y, g.z):
            valid = True
        elif validate_jacobi(g.func_y, g.x, g.y, g.z):
            valid = True
        elif validate_jacobi(g.func_z, g.x, g.y, g.z):
            valid = True
        else:
            valid = False
        if valid:
            table = g.method()
            table_len = len(table)
            return render_template('gauss-seidel.html', solve=True, table=table, solve_info=solve_info,
                                   table_len=table_len)
        else:
            msg = 'Una de las funciónes no es valida'
            flash(msg, 'warning')
            return render_template('gauss-seidel.html', solve=False, solfe_info=solve_info)
    else:
        return render_template('gauss-seidel.html', solve=False)


@application.route('/trapecio', methods=['GET', 'POST'])
def trapecio():
    if request.method == 'POST':
        values = [
            request.form.get('value-a'),
            request.form.get('value-b'),
            request.form.get('function'),
            request.form.get('iterations'),
            request.form.get('decimals')
        ]

        t = Trapecio(
            int(values[0]),
            int(values[1]),
            str(values[2]),
            int(values[3]),
            int(values[4])
        )
        t.solve_method()

        solve_info = [t.data[0], t.data[1], t.data[2]]
        return render_template('trapecio.html', solve=True, solve_info=solve_info, default_values=values)
    else:
        return render_template('trapecio.html', solve=False)

# JSON RETURN METHODS - - - - - - - - - -


@application.route('/json', methods=['POST', 'GET'])
def json():
    json_failed = "{'failed_data': '1'}"

    if request.method == 'POST':
        json_failed_method = "{'failed_data_method': '1'}" + str(request.form.get('method'))

        if request.form.get('method') == 'trapecio':
            values = [
                request.form.get('value-a'),
                request.form.get('value-b'),
                request.form.get('function'),
                request.form.get('iterations'),
                request.form.get('decimals')
            ]

            t = Trapecio(
                int(values[0]),
                int(values[1]),
                str(values[2]),
                int(values[3]),
                int(values[4])
            )
            t.solve_method()
            return t.json
        elif request.form.get('method') == 'punto_fijo':
            values = [
                request.form.get('value-init'),
                request.form.get('function'),
                request.form.get('function-g'),
                request.form.get('value-error'),
                request.form.get('decimals')
            ]

            p = PuntoFijo(
                float(values[0]),
                str(values[1]),
                str(values[2]),
                float(values[3]),
                int(values[4])
            )

            if validate(p.g, 1):
                p.solve_method()
                return p.json
            else:
                return '{"invalid_function": "1"}'
        elif request.form.get('method') == 'jacobi':
            j = Jacobi()
            j.func_x = request.form.get('function-x')
            j.func_y = request.form.get('function-y')
            j.func_z = request.form.get('function-z')
            j.x = float(request.form.get('value-x'))
            j.y = float(request.form.get('value-y'))
            j.z = float(request.form.get('value-z'))
            j.error = float(request.form.get('value-error'))
            j.decimals = int(request.form.get('decimals'))
            solve_info = [j.func_x, j.func_y, j.func_z, j.x, j.y, j.z, j.error, j.decimals]
            print('LOG: Llego hast aaquí')
            if validate_jacobi(j.func_x, j.x, j.y, j.z):
                valid = True
            elif validate_jacobi(j.func_y, j.x, j.y, j.z):
                valid = True
            elif validate_jacobi(j.func_z, j.x, j.y, j.z):
                valid = True
            else:
                valid = False
            if valid:
                table = j.method()
                return table
            else:
                return '{"invalid_functions": "1"}'
        elif request.form.get('method') == 'secante':
            values = [
                request.form.get('value-x0'),
                request.form.get('value-x1'),
                request.form.get('function'),
                request.form.get('iterations'),
                request.form.get('decimals')
            ]

            s = Secante(
                int(values[0]),
                int(values[1]),
                str(values[2]),
                int(values[3]),
                int(values[4])
            )
            s.solve_method()
            return s.json
        else:
            return json_failed_method
    else:
        return json_failed
